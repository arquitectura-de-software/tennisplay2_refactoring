
public class TennisGame2 implements TennisGame
{
    public int Player1Points = 0;
    public int Player2Points = 0;
    
    public String Player1Resp = "";
    public String Player2Resp = "";
    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore(){
        String score = "";

        score = normal();
		if (IsTie())
			score = getLiteral(Player1Points)+"-All";
		if (IsDeuce())
			score = "Deuce";
		
		if (isAdvantage(Player1Points, Player2Points))
			score = "Advantage player1";
		if (isAdvantage(Player2Points, Player1Points))
			score = "Advantage player2";
        score = win(score);
        return score;
    }

	private String win(String score) {
		if (Player1Points>=4 && Player2Points>=0 && (Player1Points-Player2Points)>=2)
        {
            score = "Win for player1";
        }
        if (Player2Points>=4 && Player1Points>=0 && (Player2Points-Player1Points)>=2)
        {
            score = "Win for player2";
        }
		return score;
	}

	private boolean isAdvantage(int firstPlayerPoints,int secondPlayerPoints) {
		return firstPlayerPoints > secondPlayerPoints && secondPlayerPoints >= 3;
	}

	private String normal() {
		String result="";
		if (Player2Points!=Player1Points)
			result =  getLiteral(Player1Points) + "-" +  getLiteral(Player2Points);
		return result;
	}
	
	private String getLiteral(int points) {
	
        if (points==1)
            return "Fifteen";
        if (points==2)
            return "Thirty";
        if (points==3)
            return "Forty";
        return "Love";
	}

	private boolean IsTie() {
		return Player1Points == Player2Points && Player1Points < 4;
	}

	private boolean IsDeuce() {
		return Player1Points==Player2Points && Player1Points>=3;
	}
    
    public void SetP1Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }
            
    }
    
    public void SetP2Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }
            
    }
    

    //CEWNIO
    public void P1Score(){
        Player1Points++;
    }

    //FUN
    public void P2Score(){
        Player2Points++;
    }

    //FUN
    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}